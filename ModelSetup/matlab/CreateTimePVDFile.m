function [] = CreateTimePVDFile(output,nstart,dn,nend)

% Creates PVD file from timestep information in case the pvd file was not
% outputted from LaMEM
%
% Structure:
%       CreateTimePVDFile(n,dn)
%
% where output   - (str) name of pvd file to be written
%       nstart   - (int) no of first timestep
%       dn       - (int) intervals between timestep
%       nend     - (int) no of last timestep
%
% Example: CreateTimePVDFile(0,20,100) will create this PVD file:

if exist(output, 'file')
  % delete if file exists
  delete(output)
end

fid   = fopen(output,'a');

% write headers
header = '<?xml version="1.0"?>\n';
fprintf(fid,header);

header = '<VTKFile type="Collection" version="0.1" byte_order="LittleEndian">\n';
fprintf(fid,header);

header = '<Collection>\n';
fprintf(fid,header);

for i=nstart:dn:nend 
    
    clearvars line line2
    
    num = num2str(i);
    if i<10
        Directory = strcat('Timestep_00000',num);
    elseif i>9 && i<100
        Directory = strcat('Timestep_0000',num);
    elseif i>99 && i<1000
        Directory = strcat('Timestep_000',num);
    elseif i>999 && i<10000
        Directory = strcat('Timestep_00',num);
    elseif i>9999
        Directory = strcat('Timestep_0',num);
    end
    
    cd (Directory)

    % open file and read line
    fid1    = fopen('timestep.dat','r');
    line = fgets(fid1);
    fclose(fid1);
        
    cd ..
    
    line2 = ['\t' line];
    
    % write line to pvd file
    fprintf(fid,line2);
end

% write end structures
hend = '</Collection>\n';
fprintf(fid,hend);

hend = '</VTKFile>';
fprintf(fid,hend);

fclose(fid);
