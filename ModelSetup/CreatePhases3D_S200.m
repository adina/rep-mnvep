% --------------------------------------------------%
%%%%% 3D India-Asia Collision - Non-linearities %%%%%
% --------------------------------------------------%
% see setup 040 and 04C for previous definitions

% This script creates LaMEM input files (parallel and/or sequential) for markers
% Files contain: marker coordinates, phase and temperature distributions
% WARNING: The model setup should be dimensional! Non-dimensionalization is done internally in LaMEM!
% WARNING: units should be consistent with the input file
%          old_input = [m, deg C]
%          new_input (units=si ) = [m,  deg K]
%          new_input (units=geo) = [km, deg C]

clear
%addpath ../../../matlab

%==========================================================================
% OUTPUT OPTIONS
%==========================================================================
% See model setup in Paraview 1-YES; 0-NO
Paraview_output        = 0;

% Output a single file containing particles information for LaMEM (msetup = redundant)
LaMEM_Redundant_output = 0;

% Output parallel files for LaMEM, using a processor distribution file (msetup = parallel)
% WARNING: Need a valid 'Parallel_partition' file!
LaMEM_Parallel_output  = 1;

% Mesh from file 1-YES (load uniform or variable mesh from file); 0-NO (create new uniform mesh)
% WARNING: Need a valid 'Parallel_partition' file!
LoadMesh               = 1;

% Parallel partition file
Parallel_partition     = 'ProcessorPartitioning_512cpu_16.8.4.bin';

% RandomNoise
RandomNoise = logical(1);

% Avoid memory block
if (Paraview_output==1) & RandomNoise
    warning('Paraview output is not recommended for setups with random noise!')
    RandomNoise = logical(0);
end

% Output
Is64BIT     = logical(0); % if you are reading a 64 bit file (Juqueen)

% Initialize temperature
InitTemperature        = 1;
ConstantMantleT        = -1; %set -1 if not constant T 1350

% Set adiabat in mantle: dT/dz = alpha*g*T/cp;
adiabat_dTdz = 0.5; % deg C/km (approx)

%==========================================================================
% DOMAIN PARAMETERS (DIMENSIONAL) [km, deg C]
%==========================================================================
W       =   5100;
L       =   5100;
H       =   1000;

% Markers
nump_x  =   128*3;  %for simulations
nump_y  =   128*3;
nump_z  =   32*3;

% No of particles in a grid cell
npart_x = 3;
npart_y = 3;
npart_z = 3;

% Model specific parameters
dx  =   W/(nump_x);
dy  =   L/(nump_y);
dz  =   H/(nump_z);
x_left  =   -3000;
y_front =   0;
z_bot   =   0;

%Slab - related parameters
margin      =   300;               %specify whether the plates are attached/unattached to the boundaries

% Thicknesses
ThicknessOL         =   60; % oceanic lith
ThicknessCL         =  100; % continental lith
ThicknessAir        =   60; % air
ThicknessUC         =   20; % upper crust
ThicknessCC         =   40; % cont crust
ThicknessOC         =   20; % oceanic crust

% Slab
slab_depth          =   300;
Depth_LowerMantle   =   H - ThicknessAir - 660; %660km

% WEAK ZONE (subducting channel)
Width_weak          = 20;

% Angle of subduction
alpha = 45;

% positions of the continent
xs_india = -2100;
xe_india = -600;
ys_india = 1500;
ye_india = 3600;

% unattached margins
unmargin = 100;

% Control buoyancy of slab pull (thickness of subducting slab)
extra   = 10; % 0 - default
extraSL = 1.3; % 1 - default

%==========================================================================
% MESH GRID
%==========================================================================

% Create new uniform grid
if LoadMesh == 0
    x = [x_left  + dx*0.5 : dx : x_left+W  - dx*0.5 ];
    y = [y_front + dy*0.5 : dy : y_front+L - dy*0.5 ];
    z = [z_bot   + dz*0.5 : dz : z_bot+H   - dz*0.5 ];
    [X,Y,Z] =   meshgrid(x,y,z);
    [Xq,Yq] =   meshgrid(x,y);
end

% Load grid from parallel partitioning file
if LoadMesh == 1
    [Xreg,Yreg,Zreg,x,y,z,X,Y,Z] = FDSTAGMeshGeneratorMatlab(npart_x,npart_y,npart_z,Parallel_partition, RandomNoise,Is64BIT );
    [Xq,Yq] = meshgrid(Xreg(1,:,1),Yreg(:,1,1));
    
    % Update other variables
    nump_x = size(Xreg,2);
    nump_y = size(Xreg,1);
    nump_z = size(Xreg,3);
end

%==========================================================================
% PHASES
%==========================================================================

%PHASES
mantle       = 0;
air          = 1;
lower_mantle = 2;
ocean_um     = 3;
ocean_uc     = 4;
india        = 5;
india_lc     = 6;
india_uc     = 7;
asia         = 8;
asia_lc      = 9;
asia_uc      = 10;

% possible
weak_zone    = 11;
asia_block   = 13;
shear_zone   = 14;

Phase   =   zeros(size(X));     %   Contains phases

%==========================================================================
% SURFACE MAP
%==========================================================================

% LOAD IMAGE OF THE SURFACE
I=imread('./sym_30km_cont_smaller2.png');

% pixels
inump_x  =   170*3;
inump_y  =   170*3;

ix  =   nump_x;
iy  =   nump_y;

% Correct image into rgb code
for i=1:inump_y
    for j=1:inump_x
        if (I(i,j,1)>250 && I(i,j,2)>250 && I(i,j,3)>250)%white
            I(i,j,1)=255;
            I(i,j,2)=255;
            I(i,j,3)=255;
        elseif (I(i,j,1)>250 && I(i,j,2)<10 && I(i,j,3)<10)%red
            I(i,j,1)=255;
            I(i,j,2)=0;
            I(i,j,3)=0;
        elseif (I(i,j,3)>250 && I(i,j,1)<10 && I(i,j,2)<10) %blue
            I(i,j,1)=0;
            I(i,j,2)=0;
            I(i,j,3)=255;
        else %black
            I(i,j,1)=0;
            I(i,j,2)=0;
            I(i,j,3)=0;
        end
    end
end

% Create surface map
ISurface =   zeros(inump_y,inump_x);
for i=1:inump_y
    for j=1:inump_x
        if (I(i,j,1)==0 && I(i,j,3)==0) % india - black
            ISurface(i,j) = india;
        elseif (I(i,j,3)==255 && I(i,j,1)==0) % shear zone - blue
            ISurface(i,j) = shear_zone;
        elseif (I(i,j,1)==255 && I(i,j,2)==0) % strong blocks - red
            ISurface(i,j) = asia_block;
        end
    end
end

% Interpolate to new surface map
dxi  =   W/(inump_x-1);
dyi  =   L/(inump_y-1);
Ix = [x_left  : dxi : x_left+W ];
Iy = [y_front : dyi : y_front+L];

[Xi,Yi] =   meshgrid(Ix,Iy);

Surface = interp2(Xi,Yi,ISurface,Xq,Yq);

%==========================================================================
% SETUP GEOMETRY
%==========================================================================

% Subducting slab - flat side
ind         =   find(X>(x_left+unmargin) & X<0 & Z>(H-ThicknessAir-ThicknessOL+z_bot));
Phase(ind)  =   ocean_um;

ind         =   find(X>(x_left+unmargin) & X<0 & Z>(H-ThicknessAir-ThicknessOC+z_bot));
Phase(ind)  =   ocean_uc;

% Slab triangle corner on the left - MOR like feature
tanalpha    =   ThicknessOC/margin;
ind         =   find( X>(x_left+unmargin) & X<(x_left+margin+unmargin) & (Z<((H-ThicknessAir)-((X-x_left-unmargin)*tanalpha))));
Phase(ind)  =   ocean_um;

% Subducting slab - hinges
xcenter     =   0;
zcenter     =   H-ThicknessAir-ThicknessOL+z_bot;

radius      =   ThicknessOL;
ind         =   find( (X > xcenter) & Z>zcenter & ((X-xcenter).^2+(Z-zcenter).^2<radius^2));
Phase(ind)  =   ocean_uc;

radius      =   ThicknessOC*2;
ind         =   find( (X > xcenter) & Z>zcenter& ((X-xcenter).^2+(Z-zcenter).^2<radius^2));
Phase(ind)  =   ocean_um;

% Subducting slab - downgoing part
Xprime = (X-xcenter).*cosd(alpha) - (Z-zcenter).*sind(alpha);
Zprime = (X-xcenter).*sind(alpha) + (Z-zcenter).*cosd(alpha);

ind         =   find(Xprime>=(-xcenter-extra) & Zprime>=(H-ThicknessAir-extraSL*ThicknessOL+z_bot-zcenter) & Z>=(H-ThicknessAir-slab_depth+z_bot));
Phase(ind)  =   ocean_um;

ind         =   find(Xprime>=(-xcenter-extra) & Zprime>=(H-ThicknessAir-ThicknessOC+z_bot-zcenter) & Z>=(H-ThicknessAir-slab_depth+z_bot));
Phase(ind)  =   ocean_uc;

ind         =   find(Xprime>=(-xcenter-extra) & Zprime>=(H-ThicknessAir+z_bot-zcenter) & Z>=(H-ThicknessAir-slab_depth+z_bot));
Phase(ind)  =   mantle;

% Overriding plate

% Asian lithosphere
ind         =   find((Xprime>(Width_weak-xcenter-extra)) & Zprime>(H-ThicknessAir+z_bot-zcenter) & Z>(H-ThicknessAir-ThicknessCL+z_bot));
Phase(ind)  =   asia;

% Lower Crust Asia
ind         =   find((Xprime>(Width_weak-xcenter-extra)) & Zprime>(H-ThicknessAir+z_bot-zcenter) & Z>(H-ThicknessAir-ThicknessCC+z_bot));
Phase(ind)  =   asia_lc;

% Upper Crust Asia
ind         =   find((Phase==mantle | Phase==asia_lc) & Z>(H-ThicknessAir-ThicknessUC+z_bot) & X>0);
Phase(ind)  =   asia_uc;

% WEAK ZONE (SUBDUCTION CHANNEL)
ind         =   find(Xprime>(-xcenter-extra) & (Zprime<(H-2*Width_weak+z_bot-zcenter)) & Zprime>(H-ThicknessAir+z_bot-zcenter) & Z>(H-ThicknessAir-ThicknessCL+z_bot));
Phase(ind)  =   weak_zone;

ind         =   find((Phase==asia_uc) & X<=Width_weak);
Phase(ind)  =   weak_zone;

% % second option for subduction
% ind         =   find((Phase==mantle | Phase==asia_lc) & Z>(H-ThicknessAir-ThicknessUC+z_bot) & X>0);
% Phase(ind)  =   air;
% 
% ind         =   find((Xprime>(Width_weak-xcenter-extra)) & Zprime>(H-ThicknessAir+z_bot-zcenter) & Z>(H-ThicknessAir-ThicknessUC+z_bot) & X>0);
% Phase(ind)  =   asia_uc;

% Continental indentor

% India
xslope   = margin*ThicknessOL/ThicknessCL;
tanslope = ThicknessOL/xslope;

[k]     =   find((Z(1,1,:)>(H-ThicknessAir-ThicknessCL)));
for ii=1:size(k)
    for i=1:iy
        for j=1:ix
            if (Surface(i,j)==india)
                % margins
                if (X(i,j,k(ii))>(xs_india+margin) & X(i,j,k(ii))<(xe_india-margin) & Y(i,j,k(ii))>(ys_india+margin) & Y(i,j,k(ii))<(ye_india-margin))
                    Phase(i,j,k(ii))= india;
                end
                
                % left margin
                xp = X(i,j,k(ii)) - xs_india;
                if ( X(i,j,k(ii))<(xs_india+margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Y(i,j,k(ii))>(ys_india+margin) & Y(i,j,k(ii))<(ye_india-margin))
                    Phase(i,j,k(ii))= india;
                end
                
                % right margin
                xp = xe_india - X(i,j,k(ii));
                if ( X(i,j,k(ii))>(xe_india-margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Y(i,j,k(ii))>(ys_india+margin) & Y(i,j,k(ii))<(ye_india-margin))
                    Phase(i,j,k(ii))= india;
                end
                
                % front margin
                yp = Y(i,j,k(ii)) - ys_india;
                if ( Y(i,j,k(ii))<(ys_india+margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope) & X(i,j,k(ii))>(xs_india+margin) & X(i,j,k(ii))<(xe_india-margin))
                    Phase(i,j,k(ii))= india;
                end
                
                % back margin
                yp = ye_india - Y(i,j,k(ii));
                if ( Y(i,j,k(ii))>(ye_india-margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope) & X(i,j,k(ii))>(xs_india+margin) & X(i,j,k(ii))<(xe_india-margin))
                    Phase(i,j,k(ii))= india;
                end
                
                % corners 1
                xp = X(i,j,k(ii)) - xs_india;
                yp = Y(i,j,k(ii)) - ys_india;
                if (X(i,j,k(ii))<(xs_india+margin) & Y(i,j,k(ii))<(ys_india+margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope))
                    Phase(i,j,k(ii))= india;
                end
                
                % corners 2
                xp = X(i,j,k(ii)) - xs_india;
                yp = ye_india - Y(i,j,k(ii));
                if (X(i,j,k(ii))<(xs_india+margin) & Y(i,j,k(ii))>(ye_india-margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope))
                    Phase(i,j,k(ii))= india;
                end
                
                % corners 3
                xp = xe_india - X(i,j,k(ii));
                yp = Y(i,j,k(ii)) - ys_india;
                if (X(i,j,k(ii))>(xe_india-margin) & Y(i,j,k(ii))<(ys_india+margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope))
                    Phase(i,j,k(ii))= india;
                end
                
                % corners 4
                xp = xe_india - X(i,j,k(ii));
                yp = ye_india - Y(i,j,k(ii));
                if (X(i,j,k(ii))>(xe_india-margin) & Y(i,j,k(ii))>(ye_india-margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope))
                    Phase(i,j,k(ii))= india;
                end
                
            end
        end
    end
end

% Indian lower crust
xslope   = margin*ThicknessOC/ThicknessCC;
tanslope = ThicknessOC/xslope;

[k]     =   find((Z(1,1,:)>(H-ThicknessAir-ThicknessCC)));
for ii=1:size(k)
    for i=1:iy
        for j=1:ix
            if (Surface(i,j)==india)
                % margins
                if (X(i,j,k(ii))>(xs_india+margin) & X(i,j,k(ii))<(xe_india-margin) & Y(i,j,k(ii))>(ys_india+margin) & Y(i,j,k(ii))<(ye_india-margin))
                    Phase(i,j,k(ii))= india_lc;
                end
                
                % left margin
                xp = X(i,j,k(ii)) - xs_india;
                if ( X(i,j,k(ii))<(xs_india+margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Y(i,j,k(ii))>(ys_india+margin) & Y(i,j,k(ii))<(ye_india-margin))
                    Phase(i,j,k(ii))= india_lc;
                end
                
                % right margin
                xp = xe_india - X(i,j,k(ii));
                if ( X(i,j,k(ii))>(xe_india-margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Y(i,j,k(ii))>(ys_india+margin) & Y(i,j,k(ii))<(ye_india-margin))
                    Phase(i,j,k(ii))= india_lc;
                end
                
                % front margin
                yp = Y(i,j,k(ii)) - ys_india;
                if ( Y(i,j,k(ii))<(ys_india+margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope) & X(i,j,k(ii))>(xs_india+margin) & X(i,j,k(ii))<(xe_india-margin))
                    Phase(i,j,k(ii))= india_lc;
                end
                
                % back margin
                yp = ye_india - Y(i,j,k(ii));
                if ( Y(i,j,k(ii))>(ye_india-margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope) & X(i,j,k(ii))>(xs_india+margin) & X(i,j,k(ii))<(xe_india-margin))
                    Phase(i,j,k(ii))= india_lc;
                end
                
                % corners 1
                xp = X(i,j,k(ii)) - xs_india;
                yp = Y(i,j,k(ii)) - ys_india;
                if (X(i,j,k(ii))<(xs_india+margin) & Y(i,j,k(ii))<(ys_india+margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope))
                    Phase(i,j,k(ii))= india_lc;
                end
                
                % corners 2
                xp = X(i,j,k(ii)) - xs_india;
                yp = ye_india - Y(i,j,k(ii));
                if (X(i,j,k(ii))<(xs_india+margin) & Y(i,j,k(ii))>(ye_india-margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope))
                    Phase(i,j,k(ii))= india_lc;
                end
                
                % corners 3
                xp = xe_india - X(i,j,k(ii));
                yp = Y(i,j,k(ii)) - ys_india;
                if (X(i,j,k(ii))>(xe_india-margin) & Y(i,j,k(ii))<(ys_india+margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope))
                    Phase(i,j,k(ii))= india_lc;
                end
                
                % corners 4
                xp = xe_india - X(i,j,k(ii));
                yp = ye_india - Y(i,j,k(ii));
                if (X(i,j,k(ii))>(xe_india-margin) & Y(i,j,k(ii))>(ye_india-margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope))
                    Phase(i,j,k(ii))= india_lc;
                end
            end
        end
    end
end

% Indian upper crust
tanslope = ThicknessUC*xslope/margin/xslope;

[k]     =   find((Z(1,1,:)>(H-ThicknessAir-ThicknessUC)));
for ii=1:size(k)
    for i=1:iy
        for j=1:ix
            if (Surface(i,j)==india)
                                % margins
                if (X(i,j,k(ii))>(xs_india+margin) & X(i,j,k(ii))<(xe_india-margin) & Y(i,j,k(ii))>(ys_india+margin) & Y(i,j,k(ii))<(ye_india-margin))
                    Phase(i,j,k(ii))= india_uc;
                end
                
                % left margin
                xp = X(i,j,k(ii)) - xs_india;
                if ( X(i,j,k(ii))<(xs_india+margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Y(i,j,k(ii))>(ys_india+margin) & Y(i,j,k(ii))<(ye_india-margin))
                    Phase(i,j,k(ii))= india_uc;
                end
                
                % right margin
                xp = xe_india - X(i,j,k(ii));
                if ( X(i,j,k(ii))>(xe_india-margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Y(i,j,k(ii))>(ys_india+margin) & Y(i,j,k(ii))<(ye_india-margin))
                    Phase(i,j,k(ii))= india_uc;
                end
                
                % front margin
                yp = Y(i,j,k(ii)) - ys_india;
                if ( Y(i,j,k(ii))<(ys_india+margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope) & X(i,j,k(ii))>(xs_india+margin) & X(i,j,k(ii))<(xe_india-margin))
                    Phase(i,j,k(ii))= india_uc;
                end
                
                % back margin
                yp = ye_india - Y(i,j,k(ii));
                if ( Y(i,j,k(ii))>(ye_india-margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope) & X(i,j,k(ii))>(xs_india+margin) & X(i,j,k(ii))<(xe_india-margin))
                    Phase(i,j,k(ii))= india_uc;
                end
                
                % corners 1
                xp = X(i,j,k(ii)) - xs_india;
                yp = Y(i,j,k(ii)) - ys_india;
                if (X(i,j,k(ii))<(xs_india+margin) & Y(i,j,k(ii))<(ys_india+margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope))
                    Phase(i,j,k(ii))= india_uc;
                end
                
                % corners 2
                xp = X(i,j,k(ii)) - xs_india;
                yp = ye_india - Y(i,j,k(ii));
                if (X(i,j,k(ii))<(xs_india+margin) & Y(i,j,k(ii))>(ye_india-margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope))
                    Phase(i,j,k(ii))= india_uc;
                end
                
                % corners 3
                xp = xe_india - X(i,j,k(ii));
                yp = Y(i,j,k(ii)) - ys_india;
                if (X(i,j,k(ii))>(xe_india-margin) & Y(i,j,k(ii))<(ys_india+margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope))
                    Phase(i,j,k(ii))= india_uc;
                end
                
                % corners 4
                xp = xe_india - X(i,j,k(ii));
                yp = ye_india - Y(i,j,k(ii));
                if (X(i,j,k(ii))>(xe_india-margin) & Y(i,j,k(ii))>(ye_india-margin) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+xp)*tanslope) & Z(i,j,k(ii))>(H-ThicknessAir-(xslope+yp)*tanslope))
                    Phase(i,j,k(ii))= india_uc;
                end
            end
        end
    end
end

% Other features

% Shear zones
[k]     =   find((Z(1,1,:)>(H-ThicknessAir-ThicknessCL)));
for ii=1:size(k)
    for i=1:iy
        for j=1:ix
            if (Surface(i,j)==shear_zone)
                Phase(i,j,k(ii))= shear_zone;
            end
        end
    end
end

% Blocks Asia
[k]     =   find((Z(1,1,:)>(H-ThicknessAir-ThicknessCL)));
for ii=1:size(k)
    for i=1:iy
        for j=1:ix
            if (Surface(i,j)==asia_block)
                Phase(i,j,k(ii))= asia_block;
            end
        end
    end
end

% Add lower mantle
ind         =   find(Z<(Depth_LowerMantle)) ;
Phase(ind)  =   lower_mantle;

% Add triangle corner on the left
tanalpha    =   ThicknessOL/margin;
ind         =   find( (Z<((H-ThicknessAir)-((X-x_left-unmargin)*tanalpha))) & Z>(Depth_LowerMantle));
Phase(ind)  =   mantle;

% Add AIR
ind         =   find( Z>(H-ThicknessAir) );
Phase(ind)  =   air;

clearvars I ISurface lx ly Surface Xi Xq Yi Yq ind i j k

%==========================================================================
% TEMPERATURE - in Celcius
%==========================================================================
% geological temperature profile
if (InitTemperature == 1)
    
    % Set initial temperature distribution (air) - in Celcius
    Temp    =   zeros(size(X));
    
    % Find surface index
    maxind = min(find(Phase(1,1,:)==air))-1;
    
    % Set half-space cooling model in oceanic lithosphere
    % Equation is: T = T_mantle*erf(z/(2*sqrt(Kappa*age_s)))
    T_mantle = 1300; % deg C
    Kappa    = 1e-6; % m2/s
    age      = 80;   % Myr
    age_s    = age*1e6*365*24*3600; % age in s
    
    % Set continental cooling in continents - linear gradient
    T0        = 0; % surface temperature
    T_cont    = 1300; %deg C
    cont_dTdz = T_cont/ThicknessCL; %deg C/km
       
    % MANTLE ADIABAT
    dz0      = -(Z(:,:,1:maxind)-H+ThicknessAir);
    Temp(:,:,1:maxind) = (T_mantle-50)+adiabat_dTdz*dz0;
    clearvars dz0
    
    % OCEANIC LITHOSPHERE
    
    % half-space cooling model
    ind_ocean = find((Phase==ocean_uc | Phase==ocean_um) & X<=10 );
    dz0       = -(Z(ind_ocean)-H+ThicknessAir);
    Temp(ind_ocean) = T_mantle*erf(dz0*1000/(2*sqrt(Kappa*age_s)));
    clearvars dz0
    
    % corner
    ind_ocean1 = find((Phase==ocean_uc | Phase==ocean_um) & (Z-(H-ThicknessAir-ThicknessOL))>X & X>0 & Z<(H - ThicknessAir) & Z>(H-ThicknessAir-ThicknessOL));
    dz0       = -(Z(ind_ocean1)-H+ThicknessAir);
    Temp(ind_ocean1) = T_mantle*erf(dz0*1000/(2*sqrt(Kappa*age_s)));
    clearvars dz0
        
    % correct for the subducting slab
    ind_slab = find((Phase==ocean_uc | Phase==ocean_um) & X>=0);
    Xmax = max(max(X(Phase==ocean_uc)));
    
    % tan_angle = slab_depth/Xmax;
    tan_angle = tand(alpha);
    cos_angle = sqrt(1/(tan_angle^2+1));
    dx0       =   X(ind_slab)-ThicknessOL/extraSL;
    dz0       = -(Z(ind_slab)-H+ThicknessAir);
    %transform coord
    dz0       = (dz0-dx0*tan_angle)*cos_angle;
    Temp(ind_slab) = T_mantle*erf(dz0*1000/(2*sqrt(Kappa*age_s)));
    clearvars dz0
    
    % Subducting slab - hinges
    xcenter     =   0;
    zcenter     =   H-ThicknessAir-ThicknessOL+z_bot;
    radius      =   ThicknessOL;
    ind_slab = find((Phase==ocean_uc | Phase==ocean_um) & (X > xcenter) & Z>zcenter & ((X-xcenter).^2+(Z-zcenter).^2<radius^2) & ((X-xcenter)./sqrt((X-xcenter).^2+(Z-zcenter).^2)<=cosd(0.5*alpha)));
    dx0         =   X(ind_slab)-xcenter;
    dz0         =   Z(ind_slab)-zcenter;
    %transform coord
    angle     = atand(dz0./dx0);
    maxangle = max(angle);
    dz0       = (maxangle-angle)/2.5/extraSL+radius - sqrt(dx0.^2+dz0.^2);
    Temp(ind_slab) = T_mantle*erf(dz0*1000/(2*sqrt(Kappa*age_s)));
    clearvars dz0
    
    % INDIA
    % continental model
    ind_india = find(Phase==india | Phase==india_lc | Phase==india_uc);
    dz0       = -(Z(ind_india)-H+ThicknessAir);
    Temp(ind_india) = T0+cont_dTdz*dz0;
    clearvars dz0
    
    % ASIA
    % continental model
    ind_asia = find(Phase==asia | Phase==asia_lc | Phase==asia_uc | Phase==weak_zone);
    dz0       = -(Z(ind_asia)-H+ThicknessAir);
    Temp(ind_asia) = T0+cont_dTdz*dz0;
    clearvars dz0
    
    % Air
    ind_air = find(Phase==air);
    Temp(ind_air) = 0;
    
    if ConstantMantleT>0
        ind_mantle = find(Phase==mantle | Phase==lower_mantle );
        Temp(ind_mantle) = ConstantMantleT;
    end
    
else
    % Set constant initial temperature distribution - in Celcius
    Temp    =   zeros(size(X));     %   Contains temperatures
    % Temp = (H-Z)./H*0 + 0.5 + (rand(size(Z))-0.5)*0.05 + 0*1000;
end

%==========================================================================
% PREPARE DATA FOR VISUALIZATION/OUTPUT
%==========================================================================

% Prepare data for visualization/output
A = struct('W',[],'L',[],'H',[],'nump_x',[],'nump_y',[],'nump_z',[],'Phase',[],'Temp',[],'x',[],'y',[],'z',[],'npart_x',[],'npart_y',[],'npart_z',[]);

Phase       = permute(Phase,[2 1 3]);
Temp        = permute(Temp, [2 1 3]);

% Linear vectors containing coords
x = X(1,:,1);
y = Y(:,1,1);
z = Z(1,1,:);

A.W      = W;
A.L      = L;
A.H      = H;
A.nump_x = nump_x;
A.nump_y = nump_y;
A.nump_z = nump_z;
A.Phase  = Phase;
A.Temp   = Temp;
A.x      = x(:);
A.y      = y(:);
A.z      = z(:);
A.npart_x= npart_x;
A.npart_y= npart_y;
A.npart_z= npart_z;

X        = permute(X,[2 1 3]);
Y        = permute(Y,[2 1 3]);
Z        = permute(Z,[2 1 3]);

A.Xpart  =  X;
A.Ypart  =  Y;
A.Zpart  =  Z;

% SAVE DATA IN 1 FILE (redundant)
if (LaMEM_Redundant_output == 1)
    PhaseVec(1) = nump_z;
    PhaseVec(2) = nump_y;
    PhaseVec(3) = nump_x;
    PhaseVec    = [PhaseVec(:); X(:); Y(:); Z(:); Phase(:); Temp(:)];
    
    % Save data to file
    ParticleOutput  =   'MarkersInput3D.dat';
    
    PetscBinaryWrite(ParticleOutput, PhaseVec);
    
end

% Clearing up some memory for parallel partitioning
clearvars -except A Paraview_output LaMEM_Parallel_output Parallel_partition Is64BIT RandomNoise

% PARAVIEW VISUALIZATION
if (Paraview_output == 1)
    if (RandomNoise)
        FDSTAGWriteMatlab2VTK(A,'VTU_BINARY'); % vtu binary for markers
    else
        FDSTAGWriteMatlab2VTK(A,'BINARY'); % default option - for regular mesh
    end
    %FDSTAGWriteMatlab2VTK(A,'ASCII'); % for debugging only (slow)
end

% SAVE PARALLEL DATA (parallel)
if (LaMEM_Parallel_output == 1)
    FDSTAGSaveMarkersParallelMatlab(A,Parallel_partition,Is64BIT);
end

%clear data
clear
