# README

README file for reproducing results in:  
**The effect of rheological approximations in 3-D numerical simulations of subduction and collision**  
Authors: Adina E. Pusok\*, Boris J.P. Kaus, Anton A. Popov  
Tectonophysics, 2018, https://doi.org/10.1016/j.tecto.2018.04.017  
\*Corresponding author  
E-mail address: apusok@ucsd.edu (A.E. Pusok).

### Structure of Repository ###
      
Structure of extended archive (87 Gb):  

Archive_MNVEP (extended):

1. Input # Contains archive files (.tgz) of all input geometries (particle files)  
2. ModelSetup # Matlab scripts to reproduce particle files (generates data in Input/)  
3. ParamFiles # LaMEM parameter files for all simulations  
4. Results  
- Full_simulations # Archive for full simulation results for S104, S105, S113, S140  
- Single_timesteps # Archive for all simulations with 1 representative timestep output  

Structure of short archive (5.9 Mb, for reproduction):

Archive_MNVEP (short):

1. ModelSetup # Matlab scripts to reproduce particle files (generates data in Input/)  
2. ParamFiles # LaMEM parameter files for all simulations  

### Code and dependencies ###

LaMEM version and dependencies (Kaus et al., 2016)

LaMEM version used: cvi_test branch commit bec909e (March, 2016)
https://bitbucket.org/bkaus/lamem/branch/cvi_test

LaMEM source code/branches are not provided in this archive!
LaMEM and the cvi_branch can be downloaded from Bitbucket using �git clone�.

The following dependencies are needed to install LaMEM and reproduce results: petsc3.5x, gcc5 and mpi compilers.
The petsc and compiler installation details are located in: LaMEM (cvi_test)/doc/installation.

### Model Setup ###

The model setup (i.e particle files) was created in Matlab. The Matlab scripts for this study can be found in Archive\_MNVEP/ModelSetup. Executing them will produce the particle files for the different geometries needed. The particle files are also archived in Archive\_MNVEP/ModelSetup.

To unarchive the input execute from the command line in Input/:  
     tar xvzf input_S100.tgz

Warning: Simulations were performed on 512cpu, so the particle files are intended for use on 512cpus.

### Input Files ###

(LaMEM Parameter Files)

Every simulation can be reproduced by running each parameter file with the respective particle input geometry.

The input geometry obtained before is specified in each file as:  
LoadInitialParticlesDirectory  = ../../Input/MatlabInputParticles-S100

General command to run LaMEM (may change on high-performance clusters):  
mpiexec -n 512 ./<path to exec>/LaMEM -ParamFile SetupS100_NVEP100_ETA1e24.dat -restart 1

### Visualization and Post-processing ###

Archive_MNVEP/Results/ contains raw simulation data obtained with LaMEM (results):

*Full_simulations - for reference models:  
4 simulation results are archived in full: S104, S105, S113, S140. Please refer to the published paper for the model details.

*Single_timesteps:  
1 timestep of an evolved stage is archived for all simulations.

For example, to unarchive the results execute from the command line:  
tar xvzf s104.tgz

Post-processing and analysis were performed on all simulation data using ParaView and Matlab. Please contact the main author for questions and access regarding the post-processing work.

This [Youtube playlist](https://www.youtube.com/channel/UCOBlIEX2TMrTX1meSHNtr8Q/playlists?view_as=subscriber) contains some movies for the main simulations in this paper.
